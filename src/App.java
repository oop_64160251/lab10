public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Circle cir = new Circle(5);
        System.out.println(cir.toString());
        System.out.println(cir.calArea());
        System.out.println(cir.calPerimeter());

        Triangle tri = new Triangle(3, 4, 5);
        System.out.println(tri.toString());
        System.out.println(tri.calArea());
        System.out.println(tri.calPerimeter());
    }
}
